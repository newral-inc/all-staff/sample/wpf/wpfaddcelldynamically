﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data; // DataTableを使用するために追加します。

namespace WPFAddCellDynamically
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        //! "No"カラム名
        private static readonly string ColumnNo = "No";

        //! "Name"カラム名
        private static readonly string ColumnName = "Name";

        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // 初期化処理を行います。
            Init();
        }

        /**
         * @brief 初期化処理
         */
        private void Init()
        {
            // データグリッドの初期化処理を行います。
            InitDataGrid();

            // データテーブルの生成処理を行います。
            var dataTable = CreateDataTable();

            // データグリッドにデータテーブルを設定します。
            dataGrid1.DataContext = dataTable;
        }

        /**
         * @brief データグリッドの初期化を行います。
         */
        private void InitDataGrid()
        {
            // 水平スクロールバーを有効にします。
            dataGrid1.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;

            // 垂直スクロールバーを有効にします。
            dataGrid1.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;

            // カラムを追加します。
            dataGrid1.Columns.Add(CreateDataGridTextColumn("番号", ColumnNo));
            dataGrid1.Columns.Add(CreateDataGridTextColumn("名前", ColumnName));
        }

        /**
         * @brief データグリッドテキストカラムを生成します。
         * 
         * @param [in] header ヘッダーに表示する見出し
         * @param [in] path バインドするパス名
         * @return データグリッドテキストカラム
         */
        private DataGridTextColumn CreateDataGridTextColumn(string header, string path)
        {
            var dataGridTextColumn = new DataGridTextColumn()
                {
                    Header = header,
                    IsReadOnly = false,
                    FontSize = 12,
                    Binding = new Binding(path)
                };
            return dataGridTextColumn;
        }

        /**
         * @brief データテーブルを初期化します。
         */
        private DataTable CreateDataTable()
        {
            var dataTable = new DataTable("dataTable1");
            dataTable.Columns.Add(CreateDataColumn<int>(ColumnNo));
            dataTable.Columns.Add(CreateDataColumn<string>(ColumnName));

            // サンプルデータを追加します。
            for (int i = 0; i < 10; ++i)
            {
                var row = dataTable.NewRow();
                var no = i + 1;
                row[ColumnNo] = no;
                row[ColumnName] = "名前" + no.ToString();
                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        /**
         * @brief データカラムを生成します。
         * 
         * @param [in] name カラム名
         * @return データカラム
         */
        private DataColumn CreateDataColumn<T>(string name)
        {
            var dataColumn = new DataColumn(name, typeof(T));
            return dataColumn;
        }
    }
}
